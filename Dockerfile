FROM openjdk:8-jdk-alpine
WORKDIR /opt
VOLUME /tmp
EXPOSE 8030
COPY /target/*.jar /opt/stock-manager.jar
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /opt/stock-manager.jar" ]





